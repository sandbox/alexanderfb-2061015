
// Define the stages of remote form processing to prevent double-clicks.
BalancedPaymentsSubmitStatus = {
   'READY': 0,
   'PROCESSING': 1,
   'SUBMIT': 2
}

var balancedPaymentsSubmitOK = BalancedPaymentsSubmitStatus.READY;
var ccFormIDPrefix = '#edit-panes-payment-details-uc-balancedpayments-';

// Dim the new card entry fields if existing card was selected.
function balancedPaymentsDimNewForm() {
   if( jQuery(ccFormIDPrefix + 'stored').val() ) {
      jQuery(ccFormIDPrefix + 'card').attr( 'disabled', 'disabled' );
      jQuery(ccFormIDPrefix + 'exp-month').attr( 'disabled', 'disabled' );
      jQuery(ccFormIDPrefix + 'exp-year').attr( 'disabled', 'disabled' );
      jQuery(ccFormIDPrefix + 'cvc').attr( 'disabled', 'disabled' );
   } else {
      jQuery(ccFormIDPrefix + 'card').attr( 'disabled', false );
      jQuery(ccFormIDPrefix + 'exp-month').attr( 'disabled', false );
      jQuery(ccFormIDPrefix + 'exp-year').attr( 'disabled', false );
      jQuery(ccFormIDPrefix + 'cvc').attr( 'disabled', false );
   }
}

jQuery(document).ready( function() {

   balanced.init(
      Drupal.settings.balancedPayments.mpuri
   );

   // Set the stored card enabled status.
   balancedPaymentsDimNewForm();
   jQuery(ccFormIDPrefix + 'stored').change( balancedPaymentsDimNewForm );

   // Create a marker to specify if the cancel button was what caused a submit.
   jQuery('#uc-cart-checkout-form .form-submit').click( function( e ) {
      jQuery('#uc-cart-checkout-form').data( 'clicked', this.value );
   } );

   jQuery('#uc-cart-checkout-form').submit( function( event ) {

      // Always allow the cancel button.
      if( 'Cancel' == jQuery('#uc-cart-checkout-form').data( 'clicked' ) ) {
         return true;
      }

      // Always allow stored cards.
      if( jQuery(ccFormIDPrefix + 'stored').val() ) {
         return true;
      }

      // Just keep going if a token is already present.
      /* if(
         jQuery('#edit-panes-payment-details-uc-balancedpayments-token').val()
      ) {
         return true;
      } */

      // This is kind of convoluted, but if we try to use the much simpler
      // $(form).submit() paradigm, it clicks the cancel button instead.
      if( BalancedPaymentsSubmitStatus.READY == balancedPaymentsSubmitOK ) {
         // Disable to prevent double-click.
         balancedPaymentsSubmitOK = BalancedPaymentsSubmitStatus.PROCESSING;

         // Get the response token.
         var ccData = {
            'card_number': jQuery(ccFormIDPrefix + 'card').val(),
            'expiration_month': jQuery(ccFormIDPrefix + 'exp-month').val(),
            'expiration_year': jQuery(ccFormIDPrefix + 'exp-year').val(),
            'security_code': jQuery(ccFormIDPrefix + 'cvc').val()
         };
         balanced.card.create( ccData, function( response ) {
            switch( response.status ) {
               case 201:
                  // Card approved.
                  jQuery(ccFormIDPrefix + 'new').val( response.data['uri'] );
                  balancedPaymentsSubmitOK =
                     BalancedPaymentsSubmitStatus.SUBMIT;
                  jQuery('#edit-continue').click();
                  return;

               /* case 400:
                  // TODO: More user guidance.
                  console.log( response );
                  alert( 'One of the payment fields was omitted.' );
                  break; */

               default: 
                  // Create the error element.
                  if( 0 == jQuery('#uc-bp-errors').length ) {
                     jQuery('#payment-pane').append(
                        '<ul id="uc-bp-errors" style="display: none"></ul>'
                     );
                  } else {
                     jQuery('#uc-bp-errors').fadeOut().empty();
                  }

                  // Display the errors.
                  jQuery.each( response.error, function( key, value ) {
                     if( 'expiry' == key ) {
                        return;
                     }
                     jQuery('#uc-bp-errors').append(
                        '<li>' + key + ': ' + value + '</li>'
                     );
                  } );

                  jQuery('#uc-bp-errors').fadeIn();

                  // Scroll to the payment pane.
                  jQuery('html, body').animate( {
                     scrollTop: jQuery('#payment-pane').offset().top,
                  }, 1000 );
                  break;
            }

            // Re-enable submit to allow for correcting errors.
            balancedPaymentsSubmitOK = BalancedPaymentsSubmitStatus.READY;
         } );

         return false;
      } else if(
         BalancedPaymentsSubmitStatus.SUBMIT == balancedPaymentsSubmitOK
      ) {
         // OK to move on to the next page.
         return true;
      } else {
         // Processing.
         return false;
      }
   } );
} );

